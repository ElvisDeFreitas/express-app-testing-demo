var request = require('supertest');
var requireHelper = require('../require_helper');
var app = requireHelper('app');
var nock = require('nock');


describe('index route', function () {


  it('should respond with a 200 with no query parameters', function (done) {

    request(app)
      .get('/olamundo')
      .expect(200)
      .expect(/olavoce/, done);

  });

});